#!/bin/sh

case "$1" in
	"server")
		DNS=$(host -t A dns.geopeer.org|head -n1|/usr/bin/awk '{ print $NF; exit }')
		FILE='server'
		IP=$(/sbin/ip route get 8.8.8.8 | awk '{ print $NF; exit }')
		PATH_TO_PDNS_PLUGIN=$(PATH=.:/usr/libexec:/usr/local/libexec:/usr/bin which geopeer-powerdns-proxy)
		cat pdns.conf.template | sed -e "s%PATH_TO_POWERDNS_PROXY%${PATH_TO_PDNS_PLUGIN}%" > pdns.conf
		;;
	"client")
		DNS='8.8.8.8'
		FILE='client'
		IP=$(ip -f inet -o addr show $2|cut -d\  -f 7 | cut -d/ -f 1)
		;;
	*)
		echo "You are missing the role server or client"
		exit 1;
		
esac
cat sniproxy.conf.template  | sed -e "s/DNS_SERVER/${DNS}/" | sed -e "s/IP_TO_LISTEN/${IP}/" > sniproxy.$FILE.conf
