#/bin/bash

if [ "$(id -u)" != "0" ]; then
        echo "GeoPeer start script needs to be called as root" 1>&2
        exit 1
fi

if [ ! -f user_data ]; then
        echo -n 'Type your GeoPeer username: '
        read user
        echo -n 'Type your GeoPeer password: '
        read password
        echo 'Saving your data into user_data file'
        echo $user > user_data
        echo $password >> user_data
else
        echo 'Using data from user_data file, if you want to input your GeoPeer username/password, first delete file user_data'
fi
openvpn --config geopeer-client.conf --auth-user-pass user_data
