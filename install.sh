#!/bin/bash

function install_linux(){
                wget --quiet -O sniproxy.conf.template https://raw.githubusercontent.com/GeoPeer/geopeer-client/master/linux/sniproxy.conf.template
                wget --quiet -O geopeer-client.conf https://raw.githubusercontent.com/GeoPeer/geopeer-client/master/linux/geopeer-client.conf
                wget --quiet -O geopeer-client.up https://raw.githubusercontent.com/GeoPeer/geopeer-client/master/linux/geopeer-client.up
                wget --quiet -O geopeer-client.down https://raw.githubusercontent.com/GeoPeer/geopeer-client/master/linux/geopeer-client.down
                wget --quiet -O make.template.sh https://raw.githubusercontent.com/GeoPeer/geopeer-client/master/linux/make.template.sh
                wget --quiet -O geopeer.sh https://raw.githubusercontent.com/GeoPeer/geopeer-client/master/linux/geopeer.sh
                chmod +x geopeer-client.up geopeer-client.down make.template.sh geopeer.sh
                echo Please edit files geopeer-client.up and geopeer-client.down to match your paths, by default they look under /usr/bin/ directory.
}

function install_linux_proxy(){
                wget --quiet -O pdns.conf.template https://raw.githubusercontent.com/GeoPeer/geopeer-client/master/linux-proxy/pdns.conf.template
                wget --quiet -O recursor.conf https://raw.githubusercontent.com/GeoPeer/geopeer-client/master/linux-proxy/recursor.conf
                wget --quiet -O geopeer-proxy.sh https://raw.githubusercontent.com/GeoPeer/geopeer-client/master/linux-proxy/geopeer-proxy.sh
                chmod +x geopeer-proxy.sh
                ./make.template.sh server
                CURRENT=$(pwd)
                if [ -d /etc/pdns ]; then
                          pushd /etc/pdns
                          [ -f pdns.conf ] && mv -f pdns.conf pdns.conf.original
                          ln -s ${CURRENT}/pdns.conf
                          popd
                          echo 'Check /etc/pdns/pdns.conf file to confirm it points where geopeer-powerdns-proxy is.'
                fi
                
                if [ -d /etc/pdns-recursor ]; then
                          pushd /etc/pdns-recursor
                          [ -f recursor.conf ] && mv -f recursor.conf recursor.conf.original
                          ln -s ${CURRENT}/recursor.conf
                          popd
                fi

                if [ -d /etc/powerdns ]; then
                          pushd /etc/powerdns
                          [ -f pdns.conf ] && mv -f pdns.conf pdns.conf.original
                          [ -f recursor.conf ] && mv -f recursor.conf recursor.conf.original
                          ln -s ${CURRENT}/pdns.conf
                          ln -s ${CURRENT}/recursor.conf
                          popd
                          echo 'Check /etc/powerdns/pdns.conf file to confirm it points where geopeer-powerdns-proxy is.'
                fi
}

if [ -z "$1" ]; then
        echo You need to specify your platform
        echo $0 {linux\|linux-proxy}
        exit 1
fi
PLATFORM=$(echo -n $1|tr '[:upper:]' '[:lower:]')
echo You have now the latest config files.
case "$1" in
        "linux-proxy")
                install_linux
                install_linux_proxy
                ;;
        "linux")
                install_linux
                echo 'To connect, run as root: sh geopeer.sh'
                echo 'Remember GeoPeer needs openvpn and sniproxy installed.'
                echo 'OpenVPN is a very common package, your Linux distribution should have it'
                echo 'SNIProxy is not very common, if your Linux distribution doesnt have it, you can read directions at http://geopeer.org/linux/how-to-compile-sniproxy-from-source.html'
                ;;
        *)
                echo 'You need to specify your platform'
                echo $0 {linux\|linux-proxy}
                exit 1
                ;;
esac
