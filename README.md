# GeoPeer
Configurations for the GeoPeer project

## Linux
* Create a directory where you going to save configuration files, for example, mkdir ~/geopeer
* Go into that directory, for example, cd ~/geopeer
* Get the installer file, type wget https://raw.githubusercontent.com/GeoPeer/geopeer-client/master/install.sh
* Execute the installer, type sh ./install.sh linux

You will have all configuration files. You can connect to the GeoPeer by typing openvpn --config geopeer-client.conf. After that you will need to select one of the current zones available thanks to the great people like you who share and give geolocalizaiton.
