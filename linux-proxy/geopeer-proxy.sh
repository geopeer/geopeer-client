#/bin/bash

if [ "$(id -u)" != "0" ]; then
        echo "GeoPeer start script needs to be called as root" 1>&2
        exit 1
fi

hash openvpn 2>/dev/null || { 
        echo >&2 "OpenVPN (openvpn) is required. You just need to install it from your Linux distro."
        exit 1
}

hash pdns_server 2>/dev/null || { 
        echo >&2 "PowerDNS (pdns_server) is required. You just need to install it from your Linux distro."
        exit 1
}

hash pdns_recursor 2>/dev/null || { 
        echo >&2 "PowerDNS Recursor (pdns_recursor) is required. You just need to install it from your Linux distro."
        exit 1
}

hash sniproxy 2>/dev/null || {
        echo >&2 "SNIProxy (sniproxy) is required. You just need to install it from your Linux distro."
        echo >&2 "If your distribution doesnt have it, you can read our guide at"
        echo >&2 "http://geopeer.org/linux/how-to-compile-sniproxy-from-source.html"
        exit 1
}

PATH=/usr/libexec:/usr/local/libexec:. hash geopeer-powerdns-proxy 2>/dev/null || {
        echo >&2 "GeoPeer PowerDNS proxy is required."
        echo >&2 "You can get the code from"
        echo >&2 "https://github.com/GeoPeer/geopeer-dns-proxy"
        exit 1
}

if [ ! -f user_data ]; then
        echo -n 'Type your GeoPeer username: '
        read user
        echo -n 'Type your GeoPeer password: '
        read password
        echo 'Saving your data into user_data file'
        echo $user > user_data
        echo $password >> user_data
else
        echo 'Using data from user_data file, if you want to input your GeoPeer username/password, first delete file user_data'
fi
./make.template.sh server
sniproxy -c sniproxy.server.conf
pdns_recursor --daemon
pdns_server --daemon --guardian=yes
openvpn --config geopeer-client.conf --auth-user-pass user_data

echo 'Point DNS machines to the IP of this machine'
echo 'Remember to log into http://geopeer.org and select a zone, after that wait 1 minute'
echo 'and enjoy!'
